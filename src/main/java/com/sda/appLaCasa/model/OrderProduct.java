package com.sda.appLaCasa.model;

import javax.persistence.*;

@Entity
public class OrderProduct {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private Integer idOrderProduct;

    @ManyToOne
    @JoinColumn(name = "idOrder", nullable = false)
    private Order order;

    @ManyToOne
    @JoinColumn(name = "idProduct", nullable = false)
    private Product product;
    private Integer quantityOfProduct;

    public Integer getIdOrderProduct() {
        return idOrderProduct;
    }

    public void setIdOrderProduct(Integer idOrderProduct) {
        this.idOrderProduct = idOrderProduct;
    }

    public Integer getQuantityOfProduct() {
        return quantityOfProduct;
    }

    public void setQuantityOfProduct(Integer quantityOfProduct) {
        this.quantityOfProduct = quantityOfProduct;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
