package com.sda.appLaCasa.service;

import com.sda.appLaCasa.model.Category;
import com.sda.appLaCasa.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CategoryService {

    @Autowired
    public CategoryRepository categoryRepository;

    public static final Map<Integer, Category> categories = new HashMap<>();

    public List<Category> findAll(){
        List<Category> categories =new ArrayList<>();
        categoryRepository.findAll().forEach(c ->{
            categories.add(c);
        });
        return categories;
    }

    public Category findById(Integer idCategory){
        return categoryRepository.findById(idCategory).get();
    }


    public Category add(Category category){
        return categoryRepository.save(category);
    }

    public Category update(Integer idCategory, Category category){
        Optional<Category> byId = categoryRepository.findById(idCategory);
        if (byId.isPresent()){
            Category category1 = byId.get();
            category1.setNameCategory(category.getNameCategory());
            category1.setProducts(category.getProducts());

            return categoryRepository.save(category1);
        }else {
            System.out.println("Not found");
        }
        return null;
    }

    public Category delete(Integer idCategory){
        Optional<Category> byId = categoryRepository.findById(idCategory);
        if (byId.isPresent()){
            categoryRepository.deleteById(idCategory);
        }else {
            System.out.println("Not found");
        }
        return null;
    }
}
