package com.sda.appLaCasa.service;

import com.sda.appLaCasa.model.Order;
import com.sda.appLaCasa.model.Product;
import com.sda.appLaCasa.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OrderService {

    @Autowired
    public OrderRepository orderRepository;

    public static final Map<Integer, Product> products = new HashMap<>();

    public List<Order> findAll(){
        List<Order> orders =new ArrayList<>();
        orderRepository.findAll().forEach(c ->{
            orders.add(c);
        });
        return orders;
    }

    public Order findById(Integer idOrder){
        return orderRepository.findById(idOrder).get();
    }


    public Order add(Order order){
        return orderRepository.save(order);
    }

    public Order update(Integer idOrder, Order order){
        Optional<Order> byId = orderRepository.findById(idOrder);
        if (byId.isPresent()){
            Order order1 = byId.get();
            order1.setOrderProducts(order.getOrderProducts());
            order1.setOrderDate(order.getOrderDate());
            order1.setStatus(order.getStatus());
            order1.setAddress(order.getAddress());
            return orderRepository.save(order1);
        }else {
            System.out.println("Not found");
        }
        return null;
    }

    public Order delete(Integer idOrder){
        Optional<Order> byId = orderRepository.findById(idOrder);
        if (byId.isPresent()){
            orderRepository.deleteById(idOrder);
        }else {
            System.out.println("Not found");
        }
        return null;
    }



}
