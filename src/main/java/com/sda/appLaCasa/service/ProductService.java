package com.sda.appLaCasa.service;

import com.sda.appLaCasa.model.Product;
import com.sda.appLaCasa.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public static final Map<Integer, Product> products = new HashMap<>();

    public List<Product> findAll(){
        List<Product> products =new ArrayList<>();
        productRepository.findAll().forEach(c ->{
            products.add(c);
        });
        return products;
    }

    public Product findById(Integer idProduct){
        return productRepository.findById(idProduct).get();
    }


    public Product add(Product product){
        return productRepository.save(product);
    }

    public Product update(Integer idProduct, Product product){
        Optional<Product> byId = productRepository.findById(idProduct);
        if (byId.isPresent()){
            Product product1 = byId.get();
            product1.setNameProduct(product.getNameProduct());
            product1.setCategory((product.getCategory()));
            product1.setDescription(product.getDescription());
            product1.setImage(product.getImage());
            product1.setPrice(product.getPrice());
            product1.setWeight(product.getWeight());
            product1.setOrderProducts(product.getOrderProducts());

            return productRepository.save(product1);
        }else {
            System.out.println("Not found");
        }
        return null;
    }

    public Product delete(Integer idProduct){
        Optional<Product> byId = productRepository.findById(idProduct);
        if (byId.isPresent()){
            productRepository.deleteById(idProduct);
        }else {
            System.out.println("Not found");
        }
        return null;
    }


}
