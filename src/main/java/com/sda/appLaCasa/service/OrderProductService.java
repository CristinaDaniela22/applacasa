package com.sda.appLaCasa.service;

import com.sda.appLaCasa.model.OrderProduct;
import com.sda.appLaCasa.repository.OrderProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OrderProductService {


    @Autowired
    OrderProductRepository orderProductRepository;

    public static final Map<Integer, OrderProduct> orderProducts = new HashMap<>();

    public List<OrderProduct> findAll(){
        List<OrderProduct> orderProducts =new ArrayList<>();
        orderProductRepository.findAll().forEach(c ->{
            orderProducts.add(c);
        });
        return orderProducts;
    }

    public OrderProduct findById(Integer idOrderProduct){
        return orderProductRepository.findById(idOrderProduct).get();
    }


    public OrderProduct add(OrderProduct orderProduct){
        return orderProductRepository.save(orderProduct);
    }

    public OrderProduct update(Integer idOrderProduct, OrderProduct orderProduct){
        Optional<OrderProduct> byId = orderProductRepository.findById(idOrderProduct);
        if (byId.isPresent()){
            OrderProduct orderProduct1 = byId.get();
            orderProduct1.setOrder(orderProduct.getOrder());
            orderProduct1.setProduct(orderProduct.getProduct());
            orderProduct1.setQuantityOfProduct(orderProduct.getQuantityOfProduct());

            return orderProductRepository.save(orderProduct1);
        }else {
            System.out.println("Not found");
        }
        return null;
    }

    public OrderProduct delete(Integer idOrderProduct){
        Optional<OrderProduct> byId = orderProductRepository.findById(idOrderProduct);
        if (byId.isPresent()){
            orderProductRepository.deleteById(idOrderProduct);
        }else {
            System.out.println("Not found");
        }
        return null;
    }
}
