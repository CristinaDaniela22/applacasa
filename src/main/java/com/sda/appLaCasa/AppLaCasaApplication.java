package com.sda.appLaCasa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppLaCasaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppLaCasaApplication.class, args);
	}

}
