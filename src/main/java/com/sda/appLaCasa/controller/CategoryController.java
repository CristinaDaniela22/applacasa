package com.sda.appLaCasa.controller;


import com.sda.appLaCasa.model.Category;
import com.sda.appLaCasa.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Controller
@RequestMapping(path = "/api/categories")
public class CategoryController {

    @Autowired
    public CategoryService categoryService;


    @GetMapping(value = "/findAll")
    public List<Category> categories(){
        return categoryService.findAll();
    }

    @GetMapping("/findById/{idCategory}")
    public Category findCategoryById(@PathVariable(name = "idCatgory")
                                           Integer idCategory,
                                     @RequestBody Category category){
        return categoryService.findById(idCategory);
    }

    @PostMapping("/add")
    public Category addCategory(@RequestBody Category category){
        return categoryService.add(category);
    }

    @PutMapping(value = "/update/{idCategory}")
    public Category updateCategory(@PathVariable(name = "idCategory")
                                         Integer idCategory,
                                 @RequestBody Category category){
        return categoryService.update(idCategory, category);
    }

    @DeleteMapping("/delete/{idCategory}")
    public Category deleteCategory(@PathVariable(name = "idCategory") Integer idCategory){
        return categoryService.delete(idCategory);
    }
}
