package com.sda.appLaCasa.controller;

import com.sda.appLaCasa.model.OrderProduct;
import com.sda.appLaCasa.service.OrderProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Controller
@RequestMapping(path = "/api/orderProducts")
public class OrderProductController {

    @Autowired
    private OrderProductService orderProductService;

    @GetMapping(value = "/findAll")
    public List<OrderProduct> orderProducts(){
        return orderProductService.findAll();
    }

    @GetMapping("/findById/{idOrderProduct}")
    public OrderProduct findOrderProductById(@PathVariable(name = "idOrderProduct")
                                           Integer idOrderProduct,
                                             @RequestBody OrderProduct orderProduct){
        return orderProductService.findById(idOrderProduct);
    }

    @PostMapping("/add")
    public OrderProduct addOrderProduct(@RequestBody OrderProduct orderProduct){
        return orderProductService.add(orderProduct);
    }

    @PutMapping(value = "/update/{idOrderProduct}")
    public OrderProduct updateOrderProduct(@PathVariable(name = "idOrderProduct")
                                         Integer idOrderProduct,
                                           @RequestBody OrderProduct orderProduct){
        return orderProductService.update(idOrderProduct, orderProduct);
    }

    @DeleteMapping("/delete/{idOrderProduct}")
    public OrderProduct deleteProduct(@PathVariable(name = "idOrderProduct") Integer idOrderProduct){
        return orderProductService.delete(idOrderProduct);
    }
}
