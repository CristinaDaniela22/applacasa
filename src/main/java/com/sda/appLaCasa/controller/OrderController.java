package com.sda.appLaCasa.controller;


import com.sda.appLaCasa.model.Order;
import com.sda.appLaCasa.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(path = "/api/orders")
public class OrderController {

    @Autowired
    public OrderService orderService;

    @GetMapping(value = "/findAll")
    public List<Order> orders(){
        return orderService.findAll();
    }

    @GetMapping("/findById/{idOrder}")
    public Order findOrderById(@PathVariable(name = "idOrder")
                                           Integer idOrder,
                                   @RequestBody Order order){
        return orderService.findById(idOrder);
    }

    @PostMapping("/add")
    public Order addOrder(@RequestBody Order order){
        return orderService.add(order);
    }

    @PutMapping(value = "/update/{idOrder}")
    public Order updateOrder(@PathVariable(name = "idOrder")
                                         Integer idOrder,
                                 @RequestBody Order order){
        return orderService.update(idOrder, order);
    }

    @DeleteMapping("/delete/{idOrder}")
    public Order deleteOrder(@PathVariable(name = "idOrder") Integer idOrder){
        return orderService.delete(idOrder);
    }

}
