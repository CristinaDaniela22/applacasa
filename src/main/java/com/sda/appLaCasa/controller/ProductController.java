package com.sda.appLaCasa.controller;


import com.sda.appLaCasa.model.Product;
import com.sda.appLaCasa.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping(path = "/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping(value = "/findAll")
    public List<Product> products(){
        return productService.findAll();
    }

    @GetMapping("/findById/{idProduct}")
    public Product findProductById(@PathVariable(name = "idProduct")
                                           Integer idProduct,
                                   @RequestBody Product product){
        return productService.findById(idProduct);
    }

    @PostMapping("/add")
    public Product addProduct(@RequestBody Product product){
        return productService.add(product);
    }

    @PutMapping(value = "/update/{idProduct}")
    public Product updateProduct(@PathVariable(name = "idProduct")
                                         Integer idProduct,
                                 @RequestBody Product product){
        return productService.update(idProduct, product);
    }

    @DeleteMapping("/delete/{idProduct}")
    public Product deleteProduct(@PathVariable(name = "idProduct") Integer idProduct){
        return productService.delete(idProduct);
    }
}
