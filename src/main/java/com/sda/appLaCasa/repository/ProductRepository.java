package com.sda.appLaCasa.repository;

import com.sda.appLaCasa.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer> {
}
