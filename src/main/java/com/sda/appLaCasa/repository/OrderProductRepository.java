package com.sda.appLaCasa.repository;

import com.sda.appLaCasa.model.OrderProduct;
import org.springframework.data.repository.CrudRepository;

public interface OrderProductRepository extends CrudRepository<OrderProduct, Integer> {
}

