package com.sda.appLaCasa.repository;

import com.sda.appLaCasa.model.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Integer> {
}
