package com.sda.appLaCasa.repository;

import com.sda.appLaCasa.model.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository  extends CrudRepository<Category, Integer> {
}
